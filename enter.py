import boto3


AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
BUCKET = 'rabbit-hole-entrance'

try:
	sess = boto3.session.Session(
                	region_name='eu-west-1',
                	aws_access_key_id=AWS_ACCESS_KEY_ID,
                	aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

	s3 = sess.resource('s3')
	obj = s3.Object(BUCKET, 'readme.txt')
	print(obj.get()['Body'].read().decode('utf-8'))

except Exception as e:
	raise e